variable "VM_NAME" {
  default = "TestVM"
}

variable "VM_ADMIN" {
  default = "azure-admin"
}

variable "LOCATION" {
  default = "West Europe"
}

variable "DEFAULT_SSHKEY" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCiXr5TcfPEBmvGp/rV76sHbfa4cxuhESwmrkYKRfxRvHIcoSkXNSwM4FnrnVOg4OFBR8TU8zwF2wQMESG1egIGL2A4+1ePUn0sjSg3EgjyF2z8FcaHvZ8lFCoj9G4pHVGZzDEoaiis3n1dw9B2QENCnLMYjb98fjgV9ecyoXEr6KPpAE+MDmiCKyhp9YTG2W3RVJfE0JLvYTE4UjGr7JkBsam2oJDJYo61C4q2ysrrACRiE6cuuGcTYKtMsSq087yoPc+d2VvyYDXkLz/CxZ0c8VPY50N3Tm3pidOYtbJb77wcaLYm9J3WFzFAIZeNGOBo9L4r8T7L6Xr2q9pFPa68Kh9jZZcx4jwolkyL1lFaStedVwCM9tq+hdvpmsa73x/v3ZyC7YVEeCgOuDLtAZ1h+dYp1WO0CjJHAwXe24cgG/dhHlACp4K0kqIZc/zXnBNvLWuJuuKI18ucgDPkP6rZqEvdgpsj2mIhg0CPWNXJ+k2ntgoRPxz5KiSl6aYOMEs= ferico@Ferico-MacBook-Pro.local"
}
